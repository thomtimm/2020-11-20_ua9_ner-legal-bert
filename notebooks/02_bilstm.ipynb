{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "# Custom NER with a bi-LSTM in Keras"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "This notebook shows how to extract information from text documents with the deep learning framework **[tensorflow](https://www.tensorflow.org')** and its high-level API **[keras](https://www.tensorflow.org/api_docs/python/tf/keras)**: We\n",
    "\n",
    "- **prepare** the data,\n",
    "- **build** a bi-LSTM model,\n",
    "- **train** and\n",
    "- **evaluate** the model.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Load and preprocess the data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "We load one file for training and one for validation, and store the samples as a list of token-tag pairs:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def load_data(filename: str):\n",
    "    with open(filename, 'r') as file:\n",
    "        split_lines = [line[:-1].split() for line in file]\n",
    "    samples, start = [], 0\n",
    "    for end, parts in enumerate(split_lines):\n",
    "        if not parts:\n",
    "            samples.append(split_lines[start:end])\n",
    "            start = end + 1\n",
    "    if start < end:\n",
    "        samples.append(split_lines[start:end])\n",
    "    return [[(token, tag.split('-')[-1])for token, tag in sample] for sample in samples ]\n",
    "\n",
    "TRAIN_FILE = '../data/01_raw/bag.conll'\n",
    "train_samples = load_data(TRAIN_FILE)\n",
    "\n",
    "VAL_FILE = '../data/01_raw/bgh.conll'\n",
    "val_samples = load_data(VAL_FILE)\n",
    "\n",
    "len(train_samples), len(val_samples)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "train_samples[3][:14]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "We determine the set of all tags that occur, and add an extra tag `_` for untagged token:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def get_schema(samples):\n",
    "    return ['_'] + sorted({tag for sentence in samples for _, tag in sentence})\n",
    "\n",
    "schema = get_schema(train_samples + val_samples)\n",
    "print(' '.join(schema))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Next, we represent\n",
    "\n",
    "- each token by a word vector taken from a german language model provided by [spaCy](https://spacy.io).\n",
    "- each tag by its index in the schema.\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "!python -m spacy download de_core_news_md"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import spacy\n",
    "import numpy as np\n",
    "from tqdm import tqdm\n",
    "\n",
    "MAX_LEN = 50\n",
    "\n",
    "nlp = spacy.load('de_core_news_md')\n",
    "EMB_DIM = nlp.vocab.vectors_length\n",
    "\n",
    "def preprocess(schema, samples):\n",
    "    tag_index = {tag: index for index, tag in enumerate(schema)}\n",
    "    X = np.zeros((len(samples), MAX_LEN, EMB_DIM), dtype=np.float32)\n",
    "    y = np.zeros((len(samples), MAX_LEN), dtype=np.uint8)\n",
    "    vocab = nlp.vocab\n",
    "    for i, sentence in tqdm(enumerate(samples)):\n",
    "        for j, (token, tag) in enumerate(sentence[:MAX_LEN]):\n",
    "            X[i, j] = vocab.get_vector(token)\n",
    "            y[i,j] = tag_index[tag]\n",
    "    return X, y\n",
    "\n",
    "X_train, y_train = preprocess(schema, train_samples)\n",
    "X_val, y_val = preprocess(schema, val_samples)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "X_train.shape, y_train.shape"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Build the bi-LSTM model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "The high-level API [Keras](https://keras.io) of [tensorflow](https://www.tensorflow.org) allows us to\n",
    "build, train and apply a bi-LSTM model with just a few lines.\n",
    "\n",
    "The following picture summarizes the model layers involved:\n",
    "\n",
    "![](img/bilstm.svg)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from tensorflow.keras.models import Sequential\n",
    "from tensorflow.keras.layers import Bidirectional, LSTM, TimeDistributed, Dense\n",
    "\n",
    "def build_model(filters=256):\n",
    "    input_shape = (MAX_LEN, EMB_DIM)\n",
    "    bi_lstm = Bidirectional(LSTM(filters, return_sequences=True), input_shape = input_shape)\n",
    "    classifier = TimeDistributed(Dense(len(schema), activation='softmax'))\n",
    "    model = Sequential([bi_lstm, classifier])\n",
    "    model.compile(optimizer='Adam', loss='sparse_categorical_crossentropy', metrics='accuracy')\n",
    "    print(model.summary())\n",
    "    return model\n",
    "\n",
    "model = build_model()\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Train the model"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Now we have everything at hand to train our model:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import pandas as pd\n",
    "\n",
    "def train(model, nr_samples=-1, epochs=5, batch_size=32):\n",
    "    history = model.fit(X_train[:nr_samples], y_train[:nr_samples],\n",
    "                        validation_split=0.2, epochs=epochs, batch_size=batch_size)\n",
    "    return model, pd.DataFrame(history.history)\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "model, history = train(model)\n",
    "print(history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let us visualize the training history:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import matplotlib.pyplot as plt\n",
    "%matplotlib inline\n",
    "\n",
    "def plot_history(history):\n",
    "    _, (ax1, ax2) = plt.subplots(1,2, figsize=(15,5))\n",
    "    history[['loss', 'val_loss']].plot.line(ax=ax1)\n",
    "    history[['accuracy', 'val_accuracy']].plot.line(ax=ax2)\n",
    "\n",
    "plot_history(history)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "## Evaluation"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "To evaluate the performance of our model, we apply it to our validation data.\n",
    "\n",
    "For each sample and each token, we \n",
    "\n",
    "- obtain a probability distribution on the schema (see `y_probs`),\n",
    "- take the tag with maximal probability as the prediction (see `y_pred`),\n",
    "- store the token, the true tag and the predicted tag (see `pred_samples`).\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "def predict(model):\n",
    "    y_probs = model.predict(X_val)\n",
    "    y_pred = np.argmax(y_probs, axis=-1)\n",
    "    pred_samples = [\n",
    "        [(token, tag, schema[index]) for (token, tag), index in zip(sentence, tag_pred)]\n",
    "        for sentence, tag_pred in zip(val_samples, y_pred)\n",
    "    ]\n",
    "    return pred_samples\n",
    "\n",
    "pred_samples = predict(model)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's see what the predictions look like:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "pd.DataFrame.from_records(pred_samples[8][:30], columns=['Token', 'Truth', 'Prediction'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "To **evaluate** our model, we consider \n",
    "\n",
    "- for each tag\n",
    "- the one-vs-all binary classification problem \n",
    "- of marking every token by this one tag or any other, \n",
    "\n",
    "\n",
    "and look at the *recall*, *precision* and *f1-score* of this binary classification problem.\n",
    "\n",
    "All this is already implemented in scikit-learn:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "from sklearn.metrics import classification_report\n",
    "\n",
    "def evaluate(pred_samples):\n",
    "    y_t = [pos[1] for sentence in pred_samples for pos in sentence]\n",
    "    y_p = [pos[2]for sentence in pred_samples for pos in sentence]\n",
    "    report = classification_report(y_t, y_p, output_dict=True)\n",
    "    return pd.DataFrame.from_dict(report).transpose().reset_index()\n",
    "\n",
    "scores = evaluate(pred_samples)\n",
    "print(scores)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": false
   },
   "source": [
    "Let's also plot the scores:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "jupyter": {
     "outputs_hidden": false
    }
   },
   "outputs": [],
   "source": [
    "import altair as alt\n",
    "\n",
    "def plot(scores):\n",
    "    base =  alt.Chart(scores[:-4]).encode(x=alt.X('support', scale=alt.Scale(type='log')), y='f1-score',\n",
    "                                          tooltip=['index', 'support', 'recall', 'precision', 'f1-score'])\n",
    "    return base.mark_point().encode(color='index') + base.mark_text().encode(text='index')\n",
    "\n",
    "plot(scores)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "argv": [
    "python",
    "-m",
    "ipykernel_launcher",
    "-f",
    "{connection_file}"
   ],
   "display_name": "Python 3",
   "env": null,
   "interrupt_mode": "signal",
   "language": "python",
   "metadata": null,
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.9"
  },
  "name": "02_bilstm.ipynb"
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
